package com.skrumble.sdk_android;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.skrumble.android_sdk.network.AuthNetworking;
import com.skrumble.android_sdk.network.ChatNetworking;
import com.skrumble.android_sdk.network.callback.SimpleApiCallBack;

import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        final String email = "";
        final String password = "";
        findViewById(R.id.image_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AuthNetworking.login(email, password, new SimpleApiCallBack<JSONObject>() {
                    @Override
                    public void onApiResponse(JSONObject result, Boolean success, int statusCode) {

                    }
                });
            }
        });


        findViewById(R.id.image_button2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChatNetworking.getAllChat(new SimpleApiCallBack() {
                    @Override
                    public void onApiResponse(Object result, Boolean success, int statusCode) {

                    }
                });
            }
        });
    }
}

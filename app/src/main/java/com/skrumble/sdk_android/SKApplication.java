package com.skrumble.sdk_android;

import android.app.Application;

import com.skrumble.android_sdk.SkrumbleSDK;

public class SKApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        SkrumbleSDK.ConfigApi("auth_server", "api_server", "client_id","client_secret", "socket_server");
        SkrumbleSDK.getInstance().start();
    }
}

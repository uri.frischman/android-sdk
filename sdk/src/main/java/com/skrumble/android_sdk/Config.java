package com.skrumble.android_sdk;

public class Config {

    private static String AUTH_SERVER = "";
    private static String API_SERVER = "";
    private static String CLIENT_ID = "";
    private static String CLIENT_SECRET = "";
    private static String SOCKET_SERVER = "";

    private static int SOCKET_RECONNECT_INTERVAL = 3000;
    private static String AUTH_STRING = "";
    private static String REFRESH_TOKEN = "";
    public static final String GRANT_TYPE_PASSWORD = "password";
    public static final String GRANT_TYPE_REFRESH_TOKEN = "refresh_token";

    public static String getAuthServer() {
        return AUTH_SERVER;
    }
    public static void setAuthServer(String authServer) {
        AUTH_SERVER = authServer;
    }

    public static String getApiServer() {
        return API_SERVER;
    }
    public static void setApiServer(String apiServer) {
        API_SERVER = apiServer;
    }

    public static String getClientId() {
        return CLIENT_ID;
    }
    public static void setClientId(String clientId) {
        CLIENT_ID = clientId;
    }

    public static String getClientSecret() {
        return CLIENT_SECRET;
    }
    public static void setClientSecret(String clientSecret) {
        CLIENT_SECRET = clientSecret;
    }

    public static String getSocketServer() {
        return SOCKET_SERVER;
    }
    public static void setSocketServer(String socketServer) {
        SOCKET_SERVER = socketServer;
    }

    public static int getSocketReconnectInterval() {
        return SOCKET_RECONNECT_INTERVAL;
    }
    public static void setSocketReconnectInterval(int socketReconnectInterval) {
        SOCKET_RECONNECT_INTERVAL = socketReconnectInterval;
    }

    public static String getAuthString() {
        return String.format("Bearer %s", AUTH_STRING);
    }
    public static void setAuthString(String authString) {
        AUTH_STRING = authString;
    }

    public static String getRefreshToken() {
        return REFRESH_TOKEN;
    }

    public static void setRefreshToken(String refreshToken) {
        REFRESH_TOKEN = refreshToken;
    }
}
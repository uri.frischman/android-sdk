package com.skrumble.android_sdk;

import com.skrumble.android_sdk.network.AuthNetworking;
import com.skrumble.android_sdk.network.callback.SimpleApiCallBack;
import com.skrumble.android_sdk.network.socket.SocketManager;

import org.json.JSONObject;

public class SkrumbleSDK {

    private static SkrumbleSDK sInstance;
    private SocketManager socketManager;

    public static SkrumbleSDK getInstance(){
        if (sInstance == null){
            sInstance = new SkrumbleSDK();
        }

        return sInstance;
    }

    private SkrumbleSDK(){
        socketManager = SocketManager.getInstance();
    }

    public void start(){
        socketManager.connect();
    }

    public void stop(){
        socketManager.disconnect();
    }

    public void login(String email, String password, final SimpleApiCallBack<JSONObject> callBack){
        AuthNetworking.login(email, password, callBack);
    }

    public static void ConfigApi(String authServer, String apiServer, String clientID, String clientSecret, String sockerServer){
        Config.setAuthServer(authServer);
        Config.setApiServer(apiServer);
        Config.setClientId(clientID);
        Config.setClientSecret(clientSecret);
        Config.setSocketServer(sockerServer);
    }
}

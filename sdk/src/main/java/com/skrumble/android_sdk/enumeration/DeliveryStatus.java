package com.skrumble.android_sdk.enumeration;

import com.skrumble.android_sdk.R;

public enum DeliveryStatus {
    NotDelivered(-1, R.string.chat_message_delivery_fail),
    Sending(0, R.string.chat_message_delivery_sending),
    Delivered(1, R.string.chat_message_delivery_received),
    Seen(2, R.string.chat_message_delivery_read),
    Deleting(3, R.string.chat_message_delivery_deleting);

    private final int value;
    public int statusText;

    DeliveryStatus(int i, int statusText) {
        this.value = i;
        this.statusText = statusText;
    }

    public static DeliveryStatus fromInt(int value) {
        for (DeliveryStatus b : DeliveryStatus.values()) {
            if (value == b.getValue()) {
                return b;
            }
        }
        return DeliveryStatus.values()[0];
    }

    private int getValue() {
        return this.value;
    }

}

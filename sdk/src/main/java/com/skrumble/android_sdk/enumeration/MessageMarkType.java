package com.skrumble.android_sdk.enumeration;

public enum  MessageMarkType {

    None("none"), Pin("pin"), Unread("unread");

    private String description;

    MessageMarkType(String string) {
        description = string;
    }

    public static MessageMarkType fromString(String s){
        for (MessageMarkType messageMarkType : MessageMarkType.values()){
            if (s.equals(messageMarkType.description)){
                return messageMarkType;
            }
        }

        return MessageMarkType.None;
    }

    public String getDescription() {
        return description;
    }
}

package com.skrumble.android_sdk.enumeration;

public enum MessageType {

    None("none"), Text("text"), File("file"), CallLog("call_log"), InChatNotif("in_chat_notification");

    private String description;

    MessageType(String string) {
        description = string;
    }

    public static MessageType fromString(String s){
       switch (s){
           case "text":
               return Text;
           case "file":
               return File;
           case "call_log":
               return CallLog;
           case "in_chat_notification":
               return InChatNotif;
           default:
               return None;
       }
    }

    public String getDescription() {
        return description;
    }
}
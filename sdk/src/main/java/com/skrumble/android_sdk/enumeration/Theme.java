package com.skrumble.android_sdk.enumeration;

public enum Theme {

    Dark, Light, Invalid;

    public static Theme getTheme(String theme) {
        switch (theme) {
            case "dark":
                return Dark;
            case "light":
                return Light;
            default:
                return Invalid;
        }
    }
}

package com.skrumble.android_sdk.enumeration;

public enum UserState {
    Offline("offline"),
    Online("online");

    private final String text;

    private UserState(final String text) {
        this.text = text;
    }

    public String toString() {
        return text;
    }

    public static UserState fromString(String userStateString) {

        for (UserState state : UserState.values()){
            if (state.toString().equals(userStateString)){
                return state;
            }
        }

        return Offline;
    }
}

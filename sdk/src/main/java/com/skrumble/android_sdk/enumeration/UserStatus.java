package com.skrumble.android_sdk.enumeration;

public enum UserStatus {
    Active("active"),
    Busy("busy");

    private final String text;

    private UserStatus(final String text) {
        this.text = text;
    }

    public static UserStatus fromString(String s){
        for (UserStatus status : UserStatus.values()){
            if (status.toString().equals(s)){
                return status;
            }
        }
        return Busy;
    }

    public String toString() {
        return text;
    }
}

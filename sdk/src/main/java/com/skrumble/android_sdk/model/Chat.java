package com.skrumble.android_sdk.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.skrumble.android_sdk.enumeration.ChatType;
import com.skrumble.android_sdk.helper.JsonHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class Chat extends SKObject implements Parcelable {

    private ChatType type;

    private String ownerID;
    private String chatName;
    private String purpose;
    private String url;
    private String confPin;
    private String pin;
    private String roomNumber;
    private String avatar;
    private String color;
    private String lastMessage;

    private Date createdAt;
    private Date lastSeen;
    private Date updatedAt;
    private Date lastMessageTime;

    private Boolean doNotDisturb = false;
    private Boolean favorite;
    private Boolean lock = false;

    private int unread;

    HashMap<String, TeamUser> userParticipant = new HashMap<>();


    // *********************************************************************************************
    // region Constructor

    public Chat(JSONObject object){
        super(object);

        type = ChatType.fromString(JsonHelper.getStringFromJSON(object, "type"));

        ownerID = JsonHelper.getStringFromJSON(object, "owner");
        chatName = JsonHelper.getStringFromJSON(object, "name");
        purpose = JsonHelper.getStringFromJSON(object, "purpose");
        url = JsonHelper.getStringFromJSON(object, "url");
        confPin = JsonHelper.getStringFromJSON(object, "conf_pin");
        pin = JsonHelper.getStringFromJSON(object, "pin");
        roomNumber = JsonHelper.getStringFromJSON(object, "roomNumber");
        avatar = JsonHelper.getStringFromJSON(object, "avatar");
        color = JsonHelper.getStringFromJSON(object, "color");

        createdAt = JsonHelper.getDateFromJSON(object, "created_at");
        lastSeen = JsonHelper.getDateFromJSON(object, "last_seen");
        updatedAt = JsonHelper.getDateFromJSON(object, "updated_at");

        doNotDisturb = JsonHelper.getBooleanFromJSON(object, "do_not_disturb");
        favorite = JsonHelper.getBooleanFromJSON(object, "favorite");
        lock = JsonHelper.getBooleanFromJSON(object, "locked");

        unread = JsonHelper.getIntFromJSON(object, "unread");

        userParticipant = JsonHelper.getObjectHasMapFromJSON(object, "users", TeamUser.class);
    }

    // endregion

    // *********************************************************************************************
    // region Factory

    public static ArrayList<Chat> chatArrayListFactory(JSONArray jsonArray){
        ArrayList<Chat> chats = new ArrayList<>();

        if (jsonArray == null || jsonArray.length() == 0){
            return chats;
        }

        int size = jsonArray.length();

        for (int i = 0; i < size; i++) {
            JSONObject jsonObject = null;
            try {
                jsonObject = (JSONObject) jsonArray.get(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (jsonObject != null){
                Chat chat = new Chat(jsonObject);
                chats.add(chat);
            }
        }

        return chats;
    }

    // endregion

    // *********************************************************************************************
    // region Setter Getter

    public ChatType getType() {
        return type;
    }

    public void setType(ChatType type) {
        this.type = type;
    }

    public String getOwnerID() {
        return ownerID;
    }

    public void setOwnerID(String ownerID) {
        this.ownerID = ownerID;
    }

    public String getChatName() {
        return chatName;
    }

    public void setChatName(String chatName) {
        this.chatName = chatName;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getConfPin() {
        return confPin;
    }

    public void setConfPin(String confPin) {
        this.confPin = confPin;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getLastSeen() {
        return lastSeen;
    }

    public void setLastSeen(Date lastSeen) {
        this.lastSeen = lastSeen;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getLastMessageTime() {
        return lastMessageTime;
    }

    public void setLastMessageTime(Date lastMessageTime) {
        this.lastMessageTime = lastMessageTime;
    }

    public Boolean getDoNotDisturb() {
        return doNotDisturb;
    }

    public void setDoNotDisturb(Boolean doNotDisturb) {
        this.doNotDisturb = doNotDisturb;
    }

    public Boolean getFavorite() {
        return favorite;
    }

    public void setFavorite(Boolean favorite) {
        this.favorite = favorite;
    }

    public Boolean getLock() {
        return lock;
    }

    public void setLock(Boolean lock) {
        this.lock = lock;
    }

    public int getUnread() {
        return unread;
    }

    public void setUnread(int unread) {
        this.unread = unread;
    }

    public HashMap<String, TeamUser> getUserParticipant() {
        return userParticipant;
    }

    public void setUserParticipant(HashMap<String, TeamUser> userParticipant) {
        this.userParticipant = userParticipant;
    }

    // endregion

    // *********************************************************************************************
    // region Parcel

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.type == null ? -1 : this.type.ordinal());
        dest.writeString(this.ownerID);
        dest.writeString(this.chatName);
        dest.writeString(this.purpose);
        dest.writeString(this.url);
        dest.writeString(this.confPin);
        dest.writeString(this.pin);
        dest.writeString(this.roomNumber);
        dest.writeString(this.avatar);
        dest.writeString(this.color);
        dest.writeString(this.lastMessage);
        dest.writeLong(this.createdAt != null ? this.createdAt.getTime() : -1);
        dest.writeLong(this.lastSeen != null ? this.lastSeen.getTime() : -1);
        dest.writeLong(this.updatedAt != null ? this.updatedAt.getTime() : -1);
        dest.writeLong(this.lastMessageTime != null ? this.lastMessageTime.getTime() : -1);
        dest.writeValue(this.doNotDisturb);
        dest.writeValue(this.favorite);
        dest.writeValue(this.lock);
        dest.writeInt(this.unread);
        dest.writeSerializable(this.userParticipant);
    }

    protected Chat(Parcel in) {
        int tmpType = in.readInt();
        this.type = tmpType == -1 ? null : ChatType.values()[tmpType];
        this.ownerID = in.readString();
        this.chatName = in.readString();
        this.purpose = in.readString();
        this.url = in.readString();
        this.confPin = in.readString();
        this.pin = in.readString();
        this.roomNumber = in.readString();
        this.avatar = in.readString();
        this.color = in.readString();
        this.lastMessage = in.readString();
        long tmpCreatedAt = in.readLong();
        this.createdAt = tmpCreatedAt == -1 ? null : new Date(tmpCreatedAt);
        long tmpLastSeen = in.readLong();
        this.lastSeen = tmpLastSeen == -1 ? null : new Date(tmpLastSeen);
        long tmpUpdatedAt = in.readLong();
        this.updatedAt = tmpUpdatedAt == -1 ? null : new Date(tmpUpdatedAt);
        long tmpLastMessageTime = in.readLong();
        this.lastMessageTime = tmpLastMessageTime == -1 ? null : new Date(tmpLastMessageTime);
        this.doNotDisturb = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.favorite = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.lock = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.unread = in.readInt();
        this.userParticipant = (HashMap<String, TeamUser>) in.readSerializable();
    }

    public static final Parcelable.Creator<Chat> CREATOR = new Parcelable.Creator<Chat>() {
        @Override
        public Chat createFromParcel(Parcel source) {
            return new Chat(source);
        }

        @Override
        public Chat[] newArray(int size) {
            return new Chat[size];
        }
    };

    // endregion
}
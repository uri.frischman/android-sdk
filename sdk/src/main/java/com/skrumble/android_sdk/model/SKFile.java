package com.skrumble.android_sdk.model;

import android.os.Parcel;

import com.skrumble.android_sdk.helper.JsonHelper;

import org.json.JSONObject;

import java.util.Date;

public class SKFile extends SKObject {

    private Date createAtDate;
    private String filename;
    private String url;
    private String extension;
    private long fileSize;

    // *********************************************************************************************
    // region Constructor

    public SKFile(JSONObject jsonObject) {
        super(jsonObject);

        createAtDate = JsonHelper.getDateFromJSON(jsonObject, "created_at");
        filename = JsonHelper.getStringFromJSON(jsonObject, "filename");
        url = JsonHelper.getStringFromJSON(jsonObject, "url");
        extension = JsonHelper.getStringFromJSON(jsonObject, "extension");

        fileSize = JsonHelper.getIntFromJSON(jsonObject, "size");
    }

    // endregion

    // *********************************************************************************************
    // region Setter Getter

    public Date getCreateAtDate() {
        return createAtDate;
    }

    public void setCreateAtDate(Date createAtDate) {
        this.createAtDate = createAtDate;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    // endregion

    // *********************************************************************************************
    // region Parcel

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeLong(this.createAtDate != null ? this.createAtDate.getTime() : -1);
        dest.writeString(this.filename);
        dest.writeString(this.url);
        dest.writeString(this.extension);
        dest.writeLong(this.fileSize);
    }

    protected SKFile(Parcel in) {
        super(in);
        long tmpCreateAtDate = in.readLong();
        this.createAtDate = tmpCreateAtDate == -1 ? null : new Date(tmpCreateAtDate);
        this.filename = in.readString();
        this.url = in.readString();
        this.extension = in.readString();
        this.fileSize = in.readLong();
    }

    public static final Creator<SKFile> CREATOR = new Creator<SKFile>() {
        @Override
        public SKFile createFromParcel(Parcel source) {
            return new SKFile(source);
        }

        @Override
        public SKFile[] newArray(int size) {
            return new SKFile[size];
        }
    };

    // endregion
}
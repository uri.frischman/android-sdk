package com.skrumble.android_sdk.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.common.base.Optional;
import com.skrumble.android_sdk.helper.JsonHelper;

import org.json.JSONObject;

/**
 * Base class for model
 */
public class SKObject implements Parcelable{

    /**
     * Every object has unique id.
     */
    private String id;

    // *********************************************************************************************
    // region Constructor

    /**
     * Constructor
     */
    public SKObject(){
        id = "";
    }

    /**
     * Constructor : Create object from JSONObject
     * @param object
     */
    public SKObject(JSONObject object){
        id = JsonHelper.getStringFromJSON(object, "id");
    }

    /**
     * * Constructor : Create object from string
     * @param string
     */
    public SKObject(String string){
        id = string;
    }

    // endregion

    // *********************************************************************************************
    // region Setter Getter

    /**
     * Get unique id of object
     * @return id in Type String.
     */
    public String getId() {
        return Optional.fromNullable(id).or("");
    }

    /**
     * Set id to object
     * @param s The new id to set.
     */
    public void setId(String s){
        id = s;
    }

    // endregion

    /**
     * Compare object.
     * @param obj Object want to compare.
     * @return True if this and Object is same.
     */
    @Override
    public boolean equals(Object obj) {
        return ((SKObject)obj).getId().equals(this.id);
    }

    // *********************************************************************************************
    // region Parcel

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
    }

    protected SKObject(Parcel in) {
        this.id = in.readString();
    }

    // endregion

    // *********************************************************************************************
    // region Utility

    public boolean isActiveSKObject(){
        return true;
    }

    // endregion
}
package com.skrumble.android_sdk.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.skrumble.android_sdk.enumeration.ActivateState;
import com.skrumble.android_sdk.enumeration.Role;
import com.skrumble.android_sdk.enumeration.Theme;
import com.skrumble.android_sdk.helper.JsonHelper;

import org.json.JSONObject;

import java.util.ArrayList;

public class Team extends SKObject {

    private int billingId;

    private String name;
    private String slug;
    private String position;
    private String firstName;
    private String lastName;
    public String color;
    private String avatar;
    private String thumbUrl;
    private String email;
    private String lines;
    private String timeZone;
    private String holdMusic;

    private Address address;

    private ArrayList<TeamUser> users;
    private ArrayList<String> extension;

    private Theme theme;
    private ActivateState activateState;
    private Role role;

    // *********************************************************************************************
    // region Constructor

    public Team(JSONObject object){
        super(object);

        billingId = JsonHelper.getIntFromJSON(object, "");

        name = JsonHelper.getStringFromJSON(object, "");
        slug = JsonHelper.getStringFromJSON(object, "");
        position = JsonHelper.getStringFromJSON(object, "");
        firstName = JsonHelper.getStringFromJSON(object, "");
        lastName = JsonHelper.getStringFromJSON(object, "");
        color = JsonHelper.getStringFromJSON(object, "");
        avatar = JsonHelper.getStringFromJSON(object, "");
        thumbUrl = JsonHelper.getStringFromJSON(object, "");
        email = JsonHelper.getStringFromJSON(object, "");
        lines = JsonHelper.getStringFromJSON(object, "");
        timeZone = JsonHelper.getStringFromJSON(object, "");
        holdMusic = JsonHelper.getStringFromJSON(object, "");

        address = new Address(object);

        users = JsonHelper.getObjectArrayFromJSON(object, "users", TeamUser.class);
        extension = JsonHelper.getObjectArrayFromJSON(object, "extension", String.class);

        theme = Theme.getTheme(JsonHelper.getStringFromJSON(object, "theme"));
        activateState = ActivateState.getState(JsonHelper.getStringFromJSON(object, "activated"));
        role = Role.getRole(JsonHelper.getStringFromJSON(object, "role"));

    }

    // endregion

    // *********************************************************************************************
    // region Setter Getter

    public int getBillingId() {
        return billingId;
    }

    public void setBillingId(int billingId) {
        this.billingId = billingId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getThumbUrl() {
        return thumbUrl;
    }

    public void setThumbUrl(String thumbUrl) {
        this.thumbUrl = thumbUrl;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLines() {
        return lines;
    }

    public void setLines(String lines) {
        this.lines = lines;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getHoldMusic() {
        return holdMusic;
    }

    public void setHoldMusic(String holdMusic) {
        this.holdMusic = holdMusic;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public ArrayList<TeamUser> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<TeamUser> users) {
        this.users = users;
    }

    public ArrayList<String> getExtension() {
        return extension;
    }

    public void setExtension(ArrayList<String> extension) {
        this.extension = extension;
    }

    public Theme getTheme() {
        return theme;
    }

    public void setTheme(Theme theme) {
        this.theme = theme;
    }

    public ActivateState getActivateState() {
        return activateState;
    }

    public void setActivateState(ActivateState activateState) {
        this.activateState = activateState;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    // endregion

    // *********************************************************************************************
    // region Parcel

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        super.writeToParcel(dest, flags);

        dest.writeInt(this.billingId);

        dest.writeString(this.name);
        dest.writeString(this.slug);
        dest.writeString(this.position);
        dest.writeString(this.firstName);
        dest.writeString(this.lastName);
        dest.writeString(this.color);
        dest.writeString(this.avatar);
        dest.writeString(this.thumbUrl);
        dest.writeString(this.email);
        dest.writeString(this.lines);
        dest.writeString(this.timeZone);
        dest.writeString(this.holdMusic);

        dest.writeParcelable(this.address, flags);

        dest.writeList(this.users);
        dest.writeStringList(this.extension);

        dest.writeInt(this.theme == null ? -1 : this.theme.ordinal());
        dest.writeInt(this.activateState == null ? -1 : this.activateState.ordinal());
        dest.writeInt(this.role == null ? -1 : this.role.ordinal());
    }

    private Team(Parcel in) {
        super(in);

        this.billingId = in.readInt();

        this.name = in.readString();
        this.slug = in.readString();
        this.position = in.readString();
        this.firstName = in.readString();
        this.lastName = in.readString();
        this.color = in.readString();
        this.avatar = in.readString();
        this.thumbUrl = in.readString();
        this.email = in.readString();
        this.lines = in.readString();
        this.timeZone = in.readString();
        this.holdMusic = in.readString();

        this.address = in.readParcelable(Address.class.getClassLoader());

        this.users = new ArrayList<>();
        in.readList(this.users, TeamUser.class.getClassLoader());
        this.extension = in.createStringArrayList();

        int tmpTheme = in.readInt();
        this.theme = tmpTheme == -1 ? null : Theme.values()[tmpTheme];
        int tmpActivateState = in.readInt();
        this.activateState = tmpActivateState == -1 ? null : ActivateState.values()[tmpActivateState];
        int tmpRole = in.readInt();
        this.role = tmpRole == -1 ? null : Role.values()[tmpRole];
    }

    public static final Parcelable.Creator<Team> CREATOR = new Parcelable.Creator<Team>() {
        @Override
        public Team createFromParcel(Parcel source) {
            return new Team(source);
        }

        @Override
        public Team[] newArray(int size) {
            return new Team[size];
        }
    };

    // endregion
}

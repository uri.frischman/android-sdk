package com.skrumble.android_sdk.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.skrumble.android_sdk.enumeration.UserState;
import com.skrumble.android_sdk.enumeration.UserStatus;
import com.skrumble.android_sdk.helper.JsonHelper;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * This object is used to present current user in app
 */
public class User extends Person {

    private String email;
    private String extensionSecret;
    private String language;
    private String timeZone;
    private String timeFormat;
    private String dateFormat;
    private String tour;

    private boolean toolTips;

    private UserStatus userStatus;
    private UserState userState;

    private ArrayList<Device> mDevices;
    public ArrayList<Team> mTeams;

    // *********************************************************************************************
    // region Constructor

    public User(){
        super();

        email = "";
        extensionSecret = "";
        language = "";
        timeZone = "";
        timeFormat = "";
        dateFormat = "";
        tour = "";

        toolTips = false;

        userStatus = UserStatus.Busy;
        userState = UserState.Offline;

    }

    public User(JSONObject object){
        super(object);

        email = JsonHelper.getStringFromJSON(object, "email");
        extensionSecret = JsonHelper.getStringFromJSON(object, "extensionSecret");
        language = JsonHelper.getStringFromJSON(object, "language");
        timeZone = JsonHelper.getStringFromJSON(object, "timezone");
        timeFormat = JsonHelper.getStringFromJSON(object, "timeformat");
        dateFormat = JsonHelper.getStringFromJSON(object, "dateformat");
        tour = JsonHelper.getStringFromJSON(object, "tours");

        toolTips = JsonHelper.getBooleanFromJSON(object, "");

        String userStatusString = JsonHelper.getStringFromJSON(object, "status");
        userStatus = UserStatus.fromString(userStatusString);

        String userStateString = JsonHelper.getStringFromJSON(object, "state");
        userState = UserState.fromString(userStateString);

        mDevices = JsonHelper.getObjectArrayFromJSON(object, "devices", Device.class);
    }

    // endregion

    // *********************************************************************************************
    // region Setter Getter

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getExtensionSecret() {
        return extensionSecret;
    }

    public void setExtensionSecret(String extensionSecret) {
        this.extensionSecret = extensionSecret;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getTimeFormat() {
        return timeFormat;
    }

    public void setTimeFormat(String timeFormat) {
        this.timeFormat = timeFormat;
    }

    public String getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    public String getTour() {
        return tour;
    }

    public void setTour(String tour) {
        this.tour = tour;
    }

    public boolean isToolTips() {
        return toolTips;
    }

    public void setToolTips(boolean toolTips) {
        this.toolTips = toolTips;
    }

    public UserStatus getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(UserStatus userStatus) {
        this.userStatus = userStatus;
    }

    public UserState getUserState() {
        return userState;
    }

    public void setUserState(UserState userState) {
        this.userState = userState;
    }

    public ArrayList<Device> getmDevices() {
        return mDevices;
    }

    public void setmDevices(ArrayList<Device> mDevices) {
        this.mDevices = mDevices;
    }

    // endregion

    // *********************************************************************************************
    // region Parcel

    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        @Override
        public User createFromParcel(Parcel parcel) {
            return new User(parcel);
        }

        @Override
        public User[] newArray(int i) {
            return new User[i];
        }
    };

    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);

        out.writeString(email);
        out.writeString(extensionSecret);
        out.writeString(language);
        out.writeString(timeZone);
        out.writeString(timeFormat);
        out.writeString(dateFormat);
        out.writeString(tour);

        out.writeByte((byte) (toolTips ? 1 : 0));

        out.writeString(userStatus.toString());
        out.writeString(userState.toString());

        out.writeList(mDevices);
    }

    public User(Parcel parcel){
        super(parcel);

        email = parcel.readString();
        extensionSecret = parcel.readString();
        language = parcel.readString();
        timeZone = parcel.readString();
        timeFormat = parcel.readString();
        dateFormat = parcel.readString();
        tour = parcel.readString();

        toolTips = parcel.readByte() != 0;

        userStatus = UserStatus.fromString(parcel.readString());
        userState = UserState.fromString(parcel.readString());

        mDevices = new ArrayList<>();
        parcel.readList(mDevices, Device.class.getClassLoader());
    }

    // endregion


}

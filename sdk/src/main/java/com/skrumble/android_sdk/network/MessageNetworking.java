package com.skrumble.android_sdk.network;

import com.skrumble.android_sdk.model.Message;
import com.skrumble.android_sdk.network.callback.SimpleApiCallBack;
import com.skrumble.android_sdk.network.params.MessageParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class MessageNetworking extends Networking {

    private static String getChatMessagePopulateUrlParameters() {
        return "from,file,user_mentions,room_mentions";
    }

    private static String getSingleMessageUrl(String messageID, String chatID){
        String populate = getChatMessagePopulateUrlParameters();
        return getApiServerBaseUrl("chat/" + chatID + "/messages/" + messageID + "?populate=" + populate);
    }

    private static String getSendMessageUrl(String chatID){
        return getApiServerBaseUrl("chat/" + chatID + "/messages");
    }

    private static String getMessagesUrl(MessageParams params){
        String populate = getChatMessagePopulateUrlParameters();
        return getApiServerBaseUrl("chat/%s/messages?populate=%s&%s", params.getChatId(), populate, params.toString());
    }

    // *********************************************************************************************
    // region Get

    public static void getSingleChatMessage(String chatId, String messageId, final SimpleApiCallBack<Message> callBack){

        sendGetRequest(getSingleMessageUrl(chatId, messageId), new SimpleApiCallBack() {
            @Override
            public void onApiResponse(Object result, Boolean success, int statusCode) {
                Message message = null;
                if (success) {
                    JSONObject object = (JSONObject) result;
                    message = new Message(object);
                }

                if (callBack != null){
                    callBack.onApiResponse(message, success, statusCode);
                }
            }
        });
    }

    public static void getMessages(MessageParams params, final SimpleApiCallBack<ArrayList<Message>> callBack){

        sendGetRequest(getMessagesUrl(params), new SimpleApiCallBack() {
            @Override
            public void onApiResponse(Object result, Boolean success, int statusCode) {
                ArrayList<Message> messages = new ArrayList<>();

                if (success){
                    messages = Message.messageListFactory((JSONArray) result);
                }

                if (callBack != null){
                    callBack.onApiResponse(messages, success, statusCode);
                }
            }
        });
    }

    // endregion

    // *********************************************************************************************
    // region Send

    public static void sendTextMessage(String chatID, String body, final SimpleApiCallBack callBack) {
        HashMap<String, Object> dataMap = new HashMap<>();
        dataMap.put("type", "text");
        dataMap.put("body", body);

        sendPostRequest(getSendMessageUrl(chatID), dataMap, new SimpleApiCallBack() {
            @Override
            public void onApiResponse(Object result, Boolean success, int statusCode) {
                if (statusCode == 201) {
                    JSONObject jsonObject = (JSONObject) result;
                    if (callBack != null) {
                        callBack.onApiResponse(jsonObject, true, statusCode);
                    }
                } else {
                    if (callBack != null) {
                        callBack.onApiResponse(null, false, 0);
                    }
                }
            }
        });
    }
    
    // endregion
}

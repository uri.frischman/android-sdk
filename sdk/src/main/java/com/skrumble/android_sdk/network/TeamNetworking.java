package com.skrumble.android_sdk.network;

import com.skrumble.android_sdk.network.callback.SimpleApiCallBack;

import org.json.JSONObject;

import com.skrumble.android_sdk.model.Team;
import com.skrumble.android_sdk.model.TeamUser;
import com.skrumble.android_sdk.network.callback.SimpleApiCallBack;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class TeamNetworking extends Networking {

    private static String getCreateTeamUrl() {
        return getApiServerBaseUrl("team/");
    }

    private static String getTeamUserUrl(String teamID) {
        return getApiServerBaseUrl("team/" + teamID + "/users?populate=teams,extension,role,plan&private_chats=true&limit=1000&skip=0");
    }

    public static void createTeam(String teamName, String ownerId, String country, String city, String state, String refCode, final SimpleApiCallBack<Team> callBack) {
        String url = getCreateTeamUrl();
        HashMap<String, Object> dataMap = new HashMap<>();
        dataMap.put("team_name", teamName);
        dataMap.put("owner", ownerId);
        dataMap.put("country", country);
        dataMap.put("city", city);
        dataMap.put("state", state);
        dataMap.put("ref", refCode);
        sendPostRequest(url, dataMap, new SimpleApiCallBack<JSONObject>() {
            @Override
            public void onApiResponse(JSONObject result, Boolean success, int statusCode) {
                Team team = null;

                if (success) {
                    team = new Team(result);
                }

                if (callBack != null) {
                    callBack.onApiResponse(team, success, statusCode);
                }
            }
        });
    }

    public static void getTeamUser(String teamID, final SimpleApiCallBack callBack) {

        sendGetRequest(getTeamUserUrl(teamID), new SimpleApiCallBack() {
            @Override
            public void onApiResponse(Object result, Boolean success, int statusCode) {

                ArrayList<TeamUser> teamUserArrayList = new ArrayList<>();

                if (success) {
                    teamUserArrayList = TeamUser.teamUserListFactory((JSONArray) result);
                }

                if (callBack != null) {
                    callBack.onApiResponse(teamUserArrayList, success, statusCode);
                }
            }
        });
    }
}
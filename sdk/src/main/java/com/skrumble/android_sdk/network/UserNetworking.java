package com.skrumble.android_sdk.network;

import com.skrumble.android_sdk.model.User;
import com.skrumble.android_sdk.network.callback.SimpleApiCallBack;

import org.json.JSONObject;

import java.util.HashMap;

public class UserNetworking extends Networking {

    private static String getCreateUserUrl() {
        return getApiServerBaseUrl("user/");
    }

    private static String getCurrentUserUrl(){
        return getApiServerBaseUrl("user/me?populate=teams&extension=true");
    }

    // *********************************************************************************************
    // region Create

    public static void createUser(String firstName, String lastName, String email, String password, final SimpleApiCallBack<User> callBack) {

        String url = getCreateUserUrl();
        HashMap<String, Object> dataMap = new HashMap<>();
        dataMap.put("first_name", firstName);
        dataMap.put("last_name", lastName);
        dataMap.put("email", email);
        dataMap.put("password", password);

        sendPostRequest(url, dataMap, new SimpleApiCallBack<JSONObject>() {
            @Override
            public void onApiResponse(JSONObject result, Boolean success, int statusCode) {

                User user = null;

                if (success){
                    user = new User(result);
                }

                if (callBack != null){
                    callBack.onApiResponse(user, success, statusCode);
                }
            }
        });
    }

    // endregion

   // *********************************************************************************************
   // region Get

    public static void me(final SimpleApiCallBack<User> callBack){

        sendGetRequest(getCurrentUserUrl(), new SimpleApiCallBack() {
            @Override
            public void onApiResponse(Object result, Boolean success, int statusCode) {
                User user = null;
                if (success){
                    user = new User((JSONObject) result);
                }

                if (callBack != null){
                    callBack.onApiResponse(user, success, statusCode);
                }
            }
        });
    }

   // endregion
}
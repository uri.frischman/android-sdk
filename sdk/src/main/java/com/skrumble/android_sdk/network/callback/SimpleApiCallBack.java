package com.skrumble.android_sdk.network.callback;

public interface SimpleApiCallBack<T> {
    void onApiResponse(T result, Boolean success, int statusCode);
}

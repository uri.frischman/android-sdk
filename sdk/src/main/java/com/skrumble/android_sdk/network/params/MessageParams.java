package com.skrumble.android_sdk.network.params;

public class MessageParams {

    private String chatId = "";
    private PagingParams paging = new PagingParams(30, 0);
    QueryBuilder queryBuilder = null;

    private MessageParams(){

    }

    public MessageParams(String chatId){
        this.chatId = chatId;
    }

    public String toString() {
        if (queryBuilder == null) {
            return new QueryBuilder()
                    .add("limit", paging.limit)
                    .add("skip", paging.skip)
                    .build();
        } else {
            return queryBuilder.build();
        }
    }

    public void reset(){
        chatId = "";
        paging.reset();
        queryBuilder = null;
    }

    public PagingParams getPaging() {
        return paging;
    }

    public String getChatId() {
        return chatId;
    }
}

package com.skrumble.android_sdk.network.params;

public class PagingParams {
    int limit;
    int skip;

    private int mLastSkip = -1;

    public PagingParams() {
        this(30, 0);
    }

    public PagingParams(final int limit, final int skip) {
        this.limit = limit;
        this.skip = skip;
    }

    public String toString() {
        return new QueryBuilder()
                .add("limit", limit)
                .add("skip", skip)
                .build();
    }

    public PagingParams next() {
        if (mLastSkip >= 0) {
            return this;
        }

        if (skip < 0) {
            skip = 0;
        }

        skip += limit;

        return this;
    }

    public PagingParams previous() {
        if (skip - limit < 0) {
            skip = 0;
        } else {
            skip -= limit;
        }

        return this;
    }

    public PagingParams reset() {
        skip = 0;
        mLastSkip = -1;
        return this;
    }

    public PagingParams setLastPage() {
        mLastSkip = skip;
        return this;
    }

    public boolean isLastPage() {
        return mLastSkip >= 0;
    }
}

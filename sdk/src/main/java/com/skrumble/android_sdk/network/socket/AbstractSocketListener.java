package com.skrumble.android_sdk.network.socket;

import java.util.ArrayList;
import java.util.List;

import io.socket.client.Socket;

public abstract class AbstractSocketListener<T> {
    List<T> mListeners;
    final SocketManager mSocketHandler;

    protected AbstractSocketListener(SocketManager socketHandler) {
        mSocketHandler = socketHandler;
        mSocketHandler.registerObserver(this);
        mListeners = new ArrayList();
    }

    protected void processEvent(String event, Object... args) {
        if (args.length == 0) {
            return;
        }

        SocketEvent socketEvent = new SocketEvent(event, args);

        switch (socketEvent.verb) {
            case "updated": { onUpdated(socketEvent); break; }
            case "addedTo": { onAddedTo(socketEvent); break; }
            case "removedFrom": { onRemovedFrom(socketEvent); break; }
            case "created":{ onObjectCreated(socketEvent); }
            default: {
                onUnhandledVerb(socketEvent.verb, args);
                break;
            }
        }
    }

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// region ABSTRACT: EVENTS SUBSCRIBTION

    abstract public void onSubscribeToSocketEvents(Socket socket);
    abstract public void onUnsubscribeToSocketEvents(Socket socket);

// endregion

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// region ABSTRACT: SOCKET EVENT HANDLER

    abstract void onUpdated(SocketEvent socketEvent);
    abstract void onAddedTo(SocketEvent socketEvent);
    abstract void onRemovedFrom(SocketEvent socketEvent);
    abstract void onObjectCreated(SocketEvent socketEvent);
    abstract void onUnhandledVerb(String verb, Object... args);

// endregion
}

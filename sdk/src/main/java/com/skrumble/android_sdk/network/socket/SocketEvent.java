package com.skrumble.android_sdk.network.socket;

import com.skrumble.android_sdk.helper.JsonHelper;

import org.json.JSONObject;

public class SocketEvent {

    public final String verb;
    public final String event;
    public final String attribute;
    public final String id;

    public final JSONObject response;

    private JSONObject data;
    private JSONObject previous;

    public SocketEvent(String event, Object[] args) {
        this.event = event;

        response = (JSONObject) args[0];
        verb = JsonHelper.getStringFromJSON(response, "verb");
        attribute = JsonHelper.getStringFromJSON(response, "attribute");
        id = JsonHelper.getStringFromJSON(response, "id");
    }

    public JSONObject getData() {
        if (data == null) {
            data = (JSONObject) JsonHelper.getJsonObjectFromJson(response, "data");
        }
        return data;
    }

    public JSONObject getPrevious() {
        if (previous == null) {
            previous = (JSONObject) JsonHelper.getJsonObjectFromJson(response, "previous");
        }
        return previous;
    }
}
